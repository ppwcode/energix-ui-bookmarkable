'use strict'

/**
 * We use this script every time we build our angular project.
 */
const path = require('path')
const fs = require('fs')
const { sha1Binary: sha1 } = require('./sha')

// const ngswPathJson = path.join(__dirname, '..', 'dist', 'ngsw.json')
// const ngswJson = require(ngswPathJson)

const indexName = 'index.html'
const indexPath = path.join(__dirname, '..', 'dist', indexName)
const relativeIndexURI = `./${indexName}`

const configPath = path.join(__dirname, '..', 'config.js')

const immutableBuild = process.env.immutable_build || '00000'
const bookmarkableBuild = process.env.bookmarkable_build || '00000'

// const bookmarkNgswJson = () => {
//   // configVersion is meta information; it describes the structure of `ngsw.json`, not a version of the file
//   ngswJson.timestamp = Date.now()
//   ngswJson.index = relativeIndexURI
//
//   delete ngswJson.hashTable[`/app/artifacts/main/immutable/${immutableBuild}/${indexName}`]
//
//   const indexContent = fs.readFileSync(indexPath)
//   ngswJson.hashTable[relativeIndexURI] = sha1(indexContent)
//
//   fs.writeFileSync(ngswPathJson, JSON.stringify(ngswJson, null, 4))
// }

const bookmarkIndexHtml = () => {
  //       sub(/'<script src="config.js"><\/script>'/,"<script>\n\t\t'$(printf "%q" $config)'\n\t</script>");
  const config = fs.readFileSync(configPath).toString('UTF-8')
  const bookmarkedConfig = config.replace(
    /bookmarkableBuild: '00000'/g,
    `bookmarkableBuild: '${bookmarkableBuild}'`
  )
  const indexHtml = fs.readFileSync(indexPath).toString('UTF-8')
  const bookmarkedScoringHtml = indexHtml.replace(
    /<script src="config.js"><\/script>/g,
    '<script>\n' + bookmarkedConfig + '\t</script>'
  )

  fs.writeFileSync(indexPath, bookmarkedScoringHtml)
}

const bookmark = () => {
  bookmarkIndexHtml()
  // bookmarkNgswJson()
}

bookmark()
