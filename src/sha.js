var __read =
  (undefined && undefined.__read) ||
  function (o, n) {
    var m = typeof Symbol === 'function' && o[Symbol.iterator]
    if (!m) return o
    var i = m.call(o),
      r,
      ar = [],
      e
    try {
      while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value)
    } catch (error) {
      e = { error: error }
    } finally {
      try {
        if (r && !r.done && (m = i['return'])) m.call(i)
      } finally {
        if (e) throw e.error
      }
    }
    return ar
  }

function sha1Binary (buffer) {
  var words32 = arrayBufferToWords32(buffer, Endian.Big)
  return _sha1(words32, buffer.byteLength * 8)
}

function _sha1 (words32, len) {
  var _a, _b
  var w = new Array(80)
  var _c = __read(
      [0x67452301, 0xefcdab89, 0x98badcfe, 0x10325476, 0xc3d2e1f0],
      5
    ),
    a = _c[0],
    b = _c[1],
    c = _c[2],
    d = _c[3],
    e = _c[4]
  words32[len >> 5] |= 0x80 << (24 - (len % 32))
  words32[(((len + 64) >> 9) << 4) + 15] = len
  for (var i = 0; i < words32.length; i += 16) {
    var _d = __read([a, b, c, d, e], 5),
      h0 = _d[0],
      h1 = _d[1],
      h2 = _d[2],
      h3 = _d[3],
      h4 = _d[4]
    for (var j = 0; j < 80; j++) {
      if (j < 16) {
        w[j] = words32[i + j]
      } else {
        w[j] = rol32(w[j - 3] ^ w[j - 8] ^ w[j - 14] ^ w[j - 16], 1)
      }
      var _e = __read(fk(j, b, c, d), 2),
        f = _e[0],
        k = _e[1]
      var temp = [rol32(a, 5), f, e, k, w[j]].reduce(add32)
      ;(_a = __read([d, c, rol32(b, 30), a, temp], 5)),
        (e = _a[0]),
        (d = _a[1]),
        (c = _a[2]),
        (b = _a[3]),
        (a = _a[4])
    }
    ;(_b = __read(
      [add32(a, h0), add32(b, h1), add32(c, h2), add32(d, h3), add32(e, h4)],
      5
    )),
      (a = _b[0]),
      (b = _b[1]),
      (c = _b[2]),
      (d = _b[3]),
      (e = _b[4])
  }
  return byteStringToHexString(words32ToByteString([a, b, c, d, e]))
}

function add32 (a, b) {
  return add32to64(a, b)[1]
}

function add32to64 (a, b) {
  var low = (a & 0xffff) + (b & 0xffff)
  var high = (a >>> 16) + (b >>> 16) + (low >>> 16)
  return [high >>> 16, (high << 16) | (low & 0xffff)]
}

// Rotate a 32b number left `count` position
function rol32 (a, count) {
  return (a << count) | (a >>> (32 - count))
}

var Endian = /*@__PURE__*/ (function (Endian) {
  Endian[(Endian['Little'] = 0)] = 'Little'
  Endian[(Endian['Big'] = 1)] = 'Big'
  return Endian
})({})

function fk (index, b, c, d) {
  if (index < 20) {
    return [(b & c) | (~b & d), 0x5a827999]
  }
  if (index < 40) {
    return [b ^ c ^ d, 0x6ed9eba1]
  }
  if (index < 60) {
    return [(b & c) | (b & d) | (c & d), 0x8f1bbcdc]
  }
  return [b ^ c ^ d, 0xca62c1d6]
}

function arrayBufferToWords32 (buffer, endian) {
  var words32 = Array((buffer.byteLength + 3) >>> 2)
  var view = new Uint8Array(buffer)
  for (var i = 0; i < words32.length; i++) {
    words32[i] = wordAt(view, i * 4, endian)
  }
  return words32
}

function byteAt (str, index) {
  if (typeof str === 'string') {
    return index >= str.length ? 0 : str.charCodeAt(index) & 0xff
  } else {
    return index >= str.byteLength ? 0 : str[index] & 0xff
  }
}

function wordAt (str, index, endian) {
  var word = 0
  if (endian === Endian.Big) {
    for (var i = 0; i < 4; i++) {
      word += byteAt(str, index + i) << (24 - 8 * i)
    }
  } else {
    for (var i = 0; i < 4; i++) {
      word += byteAt(str, index + i) << (8 * i)
    }
  }
  return word
}

function words32ToByteString (words32) {
  return words32.reduce(function (str, word) {
    return str + word32ToByteString(word)
  }, '')
}

function word32ToByteString (word) {
  var str = ''
  for (var i = 0; i < 4; i++) {
    str += String.fromCharCode((word >>> (8 * (3 - i))) & 0xff)
  }
  return str
}

function byteStringToHexString (str) {
  var hex = ''
  for (var i = 0; i < str.length; i++) {
    var b = byteAt(str, i)
    hex += (b >>> 4).toString(16) + (b & 0x0f).toString(16)
  }
  return hex.toLowerCase()
}

module.exports = {
  sha1Binary: sha1Binary
}
